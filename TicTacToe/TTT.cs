﻿using System;
using System.Windows.Forms;

class TTT
{
    private int[] board = new int[9];

    private bool checkForEmptyCorner()
    {
        //upper left?
        if (board[0] == 0)
        {
            board[0] = 2;
            return true;
        }

        //lower left?
        if (board[2] == 0)
        {
            board[2] = 2;
            return true;
        }

        //upper right?
        if (board[6] == 0)
        {
            board[6] = 2;
            return true;
        }

        //lower right
        if (board[8] == 0)
        {
            board[8] = 2;
            return true;
        }

        return false;
    }

    private bool checkForEmptySide()
    {
        //left
        if (board[1] == 0)
        {
            board[1] = 2;
            return true;
        }

        //bottom
        if (board[5] == 0)
        {
            board[5] = 2;
            return true;
        }

        //right
        if (board[7] == 0)
        {
            board[7] = 2;
            return true;
        }

        //top
        if (board[3] == 0)
        {
            board[3] = 2;
            return true;
        }
        return false;
    }

    private bool checkForFork(int player)
    {
        //same player controls upper left & lower right
        if (board[0] == player && board[8] == player)
        {
            //left column & bottom row open
            if (board[1] == 0 && board[2] == 0 && board[5] == 0)
            {
                if (player == 2) board[2] = 2;
                else board[1] = 2;
                return true;
            }
            //right column & top row open
            if (board[3] == 0 && board[6] == 0 && board[7] == 0)
            {
                if (player == 2) board[6] = 2;
                else board[3] = 2;
                return true;
            }
        }

        //same player controls lower left & upper right
        if (board[2] == player && board[6] == player)
        {
            //left column & top row open
            if (board[1] == 0 && board[0] == 0 && board[3] == 0)
            {
                if (player == 2) board[0] = 2;
                else board[1] = 2;
                return true;
            }
            //right column & bottom row open
            if (board[5] == 0 && board[8] == 0 && board[7] == 0)
            {
                if (player == 2) board[8] = 2;
                else board[5] = 2;
                return true;
            }
        }

        //player can make fork with top row & left column
        if (board[0] == 0 && board[2] == 0 && board[6] == 0 && board[1] == player && board[3] == player)
        {
            board[0] = 2;
            return true;
        }

        //player can make fork with top row and right column
        if (board[6] == 0 && board[0] == 0 && board[8] == 0 && board[3] == player && board[7] == player)
        {
            board[6] = 2;
            return true;
        }

        //player can make fork with bottom row and right column
        if (board[8] == 0 && board[2] == 0 && board[6] == 0 && board[7] == player && board[5] == player)
        {
            board[8] = 2;
            return true;
        }

        //player can make fork with bottom row and left column
        if (board[2] == 0 && board[0] == 0 && board[8] == 0 && board[1] == player && board[5] == player)
        {
            board[2] = 2;
            return true;
        }
        return false;
    }

    private bool checkOppositeCorner()
    {
        //take lower right
        if (board[0] == 1 && board[8] == 0)
        {
            board[8] = 2;
            return true;
        }

        //take upper right
        if (board[2] == 1 && board[6] == 0)
        {
            board[6] = 2;
            return true;
        }

        //take upper left
        if (board[8] == 1 && board[0] == 0)
        {
            board[0] = 2;
            return true;
        }

        //take lower left
        if (board[6] == 1 && board[2] == 0)
        {
            board[2] = 2;
            return true;
        }

        return false;
    }

    public bool checkWinCondition()
    {
        //check first column
        if (board[0] == board[1] && board[1] == board[2] && board[2] != 0) return true;

        //check second column
        if (board[3] == board[4] && board[4] == board[5] && board[5] != 0) return true;

        //check last column
        if (board[6] == board[7] && board[7] == board[8] && board[8] != 0) return true;

        //check first row
        if (board[0] == board[3] && board[3] == board[6] && board[6] != 0) return true;

        //check middle row
        if (board[1] == board[4] && board[4] == board[7] && board[7] != 0) return true;

        //check last row
        if (board[2] == board[5] && board[5] == board[8] && board[8] != 0) return true;

        //check diagonally top left to bottom right
        if (board[0] == board[4] && board[4] == board[8] && board[8] != 0) return true;

        //check diagonally top right to bottom left
        if (board[6] == board[4] && board[4] == board[2] && board[2] != 0) return true;

        //no win conditions met
        return false;
    }

    private bool checkToWinBlock(int player)
    {
        //check for an empty postion where a player already has two in that row/column/diagonal
        if (board[0] == 0)
            if ((board[1] == player && board[2] == player) ||
                (board[3] == player && board[6] == player) ||
                (board[4] == player && board[8] == player))
            {
                board[0] = 2;
                return true;
            }

        if (board[1] == 0)
            if ((board[0] == player && board[2] == player) ||
                (board[4] == player && board[7] == player))
            {
                board[1] = 2;
                return true;
            }

        if (board[2] == 0)
            if ((board[0] == player && board[1] == player) ||
                (board[5] == player && board[8] == player) ||
                (board[4] == player && board[6] == player))
            {
                board[2] = 2;
                return true;
            }

        if (board[3] == 0)
            if ((board[4] == player && board[5] == player) ||
                (board[0] == player && board[6] == player))
            {
                board[3] = 2;
                return true;
            }

        if (board[4] == 0)
            if ((board[3] == player && board[5] == player) ||
                (board[1] == player && board[7] == player) ||
                (board[0] == player && board[8] == player) ||
                (board[2] == player && board[6] == player))
            {
                board[4] = 2;
                return true;
            }

        if (board[5] == 0)
            if ((board[3] == player && board[4] == player) ||
                (board[2] == player && board[8] == player))
            {
                board[5] = 2;
                return true;
            }

        if (board[6] == 0)
            if ((board[7] == player && board[8] == player) ||
                (board[3] == player && board[0] == player) ||
                (board[4] == player && board[2] == player))
            {
                board[6] = 2;
                return true;
            }

        if (board[7] == 0)
            if ((board[6] == player && board[8] == player) ||
                (board[4] == player && board[1] == player))
            {
                board[7] = 2;
                return true;
            }

        if (board[8] == 0)
            if ((board[6] == player && board[7] == player) ||
                (board[5] == player && board[2] == player) ||
                (board[4] == player && board[0] == player))
            {
                board[8] = 2;
                return true;
            }

        //if there is no win or block
        return false;
    }

    public bool gameAI()
    {
        if (checkToWinBlock(2)) return true; //try to win
        if (checkToWinBlock(1)) return true; //prevent losing
        if (checkForFork(2)) return true; //create fork
        if (checkForFork(1)) return true; //prevent fork
        //take center if available
        if (board[4] == 0)
        {
            board[4] = 2;
            return true;
        }
        if (checkOppositeCorner()) return true;
        if (checkForEmptyCorner()) return true;
        if (checkForEmptySide()) return true;

        return false;
    }

    public int getSquare(int num)
    {
        return board[num];
    }

    public void setSquare(int num)
    {
        board[num] = 1;
    }
}

