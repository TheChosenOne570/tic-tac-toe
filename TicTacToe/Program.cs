﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

public class MainForm : System.Windows.Forms.Form
{
    TTT game = new TTT();
    bool gameOver = false;

    public MainForm()
    {
        InitializeComponent();
        CenterToScreen();
        this.Text = "Tic Tac Toe";
    }

    public void MyPaintHandler(object sender, PaintEventArgs e)
    {
        Graphics g = e.Graphics;
    }

    private void InitializeComponent()
    {
        this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
        this.ClientSize = new System.Drawing.Size(292, 273);
        this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
        this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
    }

    private void MainForm_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
    {
        //If the game is not over and after the player successfully plays a position, make AI play
        if (!gameOver &&  makeMove(e))
        {
            //AI attempts to make move
            if (!game.gameAI())
            {
                //if no move, show tie game
                MessageBox.Show("Tie Game");
                gameOver = true;
            }

            //alert player if he/she lost
            gameOver = game.checkWinCondition();
            if (gameOver) MessageBox.Show("You lose");
        }

        Graphics g = Graphics.FromHwnd(this.Handle);

        Invalidate();
    }

    private void MainForm_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
    {
        Graphics g = e.Graphics;
        int offsetX, offsetY;

        g.DrawLine(new Pen(Color.Red, 7), 100, 50, 100, 200);
        g.DrawLine(new Pen(Color.Red, 7), 150, 50, 150, 200);
        g.DrawLine(new Pen(Color.Red, 7), 50, 100, 200, 100);
        g.DrawLine(new Pen(Color.Red, 7), 50, 150, 200, 150);

        //pen used for drawing on the board
        Pen pen = new Pen(Color.Blue, 4);

        for (int i = 0; i < 9; i++)
        {
            //find the offset of X
            if (i < 3)
                offsetX = 0;
            else if (i >= 3 && i < 6)
                offsetX = 50;
            else
                offsetX = 100;

            //find the offset of Y
            if (i % 3 == 0)
                offsetY = 0;
            else if (i % 3 == 1)
                offsetY = 50;
            else
                offsetY = 100;

            //draw each X
            if (game.getSquare(i) == 1)
            {
                g.DrawLine(pen, 65 + offsetX, 65 + offsetY, 85 + offsetX, 85 + offsetY);
                g.DrawLine(pen, 85 + offsetX, 65 + offsetY, 65 + offsetX, 85 + offsetY);
            }

            //draw each O
            else if (game.getSquare(i) == 2)
            {
                g.DrawEllipse(pen, 60 + offsetX, 60 + offsetY, 30, 30);
            }
        }
    }

    [STAThread]
    static void Main()
    {
        Application.Run(new MainForm());
    }

    private bool makeMove(System.Windows.Forms.MouseEventArgs e)
    {
        if (e.X >= 50 && e.X < 100)
        {
            if (e.Y >= 50 && e.Y < 100)
            {
                if (game.getSquare(0) == 0)
                {
                    game.setSquare(0);
                    return true;
                }
            }
            else if (e.Y > 105 && e.Y < 150)
            {
                if (game.getSquare(1) == 0)
                {
                    game.setSquare(1);
                    return true;
                }
            }
            else if (e.Y > 155 && e.Y < 200)
            {
                if (game.getSquare(2) == 0)
                {
                    game.setSquare(2);
                    return true;
                }
            }
        }
        else if (e.X > 105 && e.X < 150)
        {
            if (e.Y >= 50 && e.Y < 100)
            {
                if (game.getSquare(3) == 0)
                {
                    game.setSquare(3);
                    return true;
                }
            }
            else if (e.Y > 105 && e.Y < 150)
            {
                if (game.getSquare(4) == 0)
                {
                    game.setSquare(4);
                    return true;
                }
            }
            else if (e.Y > 155 && e.Y < 200)
            {
                if (game.getSquare(5) == 0)
                {
                    game.setSquare(5);
                    return true;
                }
            }
        }
        else if (e.X > 155 && e.X < 200)
        {
            if (e.Y >= 50 && e.Y < 100)
            {
                if (game.getSquare(6) == 0)
                {
                    game.setSquare(6);
                    return true;
                }
            }
            else if (e.Y > 105 && e.Y < 150)
            {
                if (game.getSquare(7) == 0)
                {
                    game.setSquare(7);
                    return true;
                }
            }
            else if (e.Y > 155 && e.Y < 200)
            {
                if (game.getSquare(8) == 0)
                {
                    game.setSquare(8);
                    return true;
                }
            }
        }
        return false;
    }


}